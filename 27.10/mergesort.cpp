//
// Created by mikew on 2020-10-26.
//
#include "mergesort.h"
#include <iostream>
using namespace std;

//mergesort


void msort::merge(int arr[], int beg, int mid, int end) {
    int tmp[end - beg + 1];

    int i = beg;
    int j = mid+1;
    int k = 0;

    while(i <= mid && j <= end) {
        if(arr[i] <= arr[j]) {
            tmp[k] = arr[i];
            k += 1; i += 1;
        }
        else {
            tmp[k] = arr[j];
            k += 1; j += 1;
        }
    }

    while(i <= mid) {
        tmp[k] = arr[i];
        k += 1; i += 1;
    }

    while(j <= end) {
        tmp[k] = arr[j];
        k += 1; j += 1;
    }

    for(i = beg; i <= end; i += 1) {
        arr[i] = tmp[i - beg];
    }
}


void msort::mergeSort(int arr[], int beg, int end) {

    if(beg < end) {
        int mid = (beg + end) / 2;
        mergeSort(arr, beg, mid);
        mergeSort(arr, mid+1, end);
        merge(arr, beg, mid, end);
    }
}
//heapsort

void Kopiec::heapify(int arr[], int i) {
    int n = (sizeof(arr)/2)-1;
    int greatest = arr[i];
    int l = arr[2 * i + 1];
    int r = arr[2 * i + 2];

    if (l < n && arr[l] > arr[greatest])
        greatest = l;

    if (r < n && arr[r] > arr[greatest])
        greatest = r;

    if (greatest != i) {
        swap(arr[i], arr[greatest]);

        heapify(arr, greatest);
    }
}


void Kopiec::build_heap(int arr[]) {
    for (int i = sizeof(arr)/2; i >= 1; i--) {
        heapify(arr, i);

    }
}

void Kopiec::heapsort(int arr[]) {
    int n = (sizeof(arr)/2)-1;
    build_heap(arr);
    for (int j = sizeof(arr); j >= 2; j--) {
        swap(arr[1], arr[j]);
        n = n-1;
        heapify(arr, 1);

    }

}
