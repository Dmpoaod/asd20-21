//
// Created by mikew on 2020-10-26.
//
#include <iostream>
using namespace std;
#ifndef INC_27_10_MERGESORT_H
#define INC_27_10_MERGESORT_H


class msort {
public:
    void merge(int arr[], int beg, int mid, int end);

    void mergeSort(int arr[], int beg, int end);
};

class Kopiec {
public:
    void heapify (int arr[], int i);

    void build_heap (int arr[]);

    void heapsort (int arr[]);
};

#endif //INC_27_10_MERGESORT_H
