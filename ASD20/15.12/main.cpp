#include <iostream>
#include <bits/stdc++.h>
using namespace std;
#define MAX 1001
//1.28
//zakładamy i>=0;
class structure128 {
public:
    int tab[MAX];
    int last[MAX];
    int cnt = 0;

    int search(int i, int S[]);

    int insert(int i, int S[]);

    int select(int i, int S[]);

    int zero(int tab[]);

};
    int structure128::zero(int tab[]) {

        last[0] = -58008;
        for (int i = 0; i < MAX; i++) {
            tab[i] = 0;
        }
    }

    int structure128::search(int i, int S[]) {
        if(S[i] == 1) {
            cout << i << "included in S";
        } else {
            cout << i << " not included in S";
        }
    }

    int structure128::insert(int i, int S[]) {
        S[i] = 1;
        last[cnt++] = i;
    }

    int structure128::select(int i, int S[]) {
        i = last[cnt];
        tab[i] = 0;
        cnt--;
    }

//1.29
class structure129 {
    struct List {
        struct Node {
            Node* prev;
            Node* next;
            int val;
        };
        Node* first = nullptr;
        struct ArrNode {
            Node* x;
            bool isin;
        };
        ArrNode arr[];
    };


    List* list;

    void push (int i, List q) {
        List::Node* x;
        x->val = i;
        x->next = nullptr;
        x->prev = nullptr;
        if (q.first == nullptr) {
            q.first = x;

        } else {
            x->next = q.first;
            q.first->prev = x;
            q.first = x;
        }
        q.arr[i].isin = 1;
        q.arr[i].x = x;
    }

    void pop (List q) {
       q.arr[q.first->val].isin = 0;
       q.first = q.first->next;
       q.first->prev = nullptr;
    }

    int search (int i, List q) {
        if (q.arr[i].isin == 1) {
            return 1;
        } else {
            return -1;
        }
    }

    void del (int i, List q) {
        q.arr[i].isin = 0;
        q.arr[i].x->prev->next = q.arr[i].x->next;
        q.arr[i].x->next->prev = q.arr[i].x->prev;
    }
};

//1.30

class structure130 {
    struct  List {
        struct Node {
            Node* prev;
            Node* next;
            Node* min;
            int val;
        };
        Node* first;
        Node* min;
    };

    List* list;

    void push (int v, List q) {
        List::Node* x;
        x->val = v;
        x->next = nullptr;
        x->prev = nullptr;
        x->min = nullptr;
        if (q.first == nullptr) {
            q.first = x;
            q.min = x;
            x->min = q.min;
        } else {
            x->min = q.min;
            x->next = q.first;
            q.first->prev = x;
            q.first = x;
            if (q.min->val > x->val) {
                q.min = x;
            }
        }
    }

    void pop (List q) {
        q.first = q.first->next;
        q.first->prev = nullptr;
    }

    void uptomin (List q) {
        q.min->next->prev = nullptr;
        q.min->next = nullptr;
    }
};





