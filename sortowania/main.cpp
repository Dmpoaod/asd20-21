#include <iostream>
using namespace std;

void insertionSort(int arr[], int n) { //aby wywołać proszę wpisać "insert"
    int i, x, j;
    for (i = 1; i < n; i++) {
        x = arr[i];
        j = i - 1;


        while (j >= 0 && arr[j] > x) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = x;
    }
}

void merge(int arr[], int l, int m, int r)
{
    int n1 = m - l + 1;
    int n2 = r - m;

    int L[n1], R[n2];

    for (int i = 0; i < n1; i++) {
        L[i] = arr[l + i];
    }

    for (int i = 0; i < n2; i++) {
        R[i] = arr[m + 1 + i];
    }


    int i = 0;

    int j = 0;

    int k = l;

    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        }
        else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }


    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}


void mergeSort(int arr[],int l,int r){ //aby wywołać proszę wpisać "merge"
    if(l>=r){
        return;
    }
    int m = (l+r-1)/2;
    mergeSort(arr,l,m);
    mergeSort(arr,m+1,r);
    merge(arr,l,m,r);
}


int main() {
    int n;
    cin >> n;
    int arr[n];
    for (int i = 0; i < n; i++) {
        cin >> arr[n];
    }

    string sort;
    cin >> sort;
    if (sort == "insert") {
        insertionSort(arr, n);
    } else if (sort == "merge") {
        mergeSort(arr, 0, n-1);

    }
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }



}
