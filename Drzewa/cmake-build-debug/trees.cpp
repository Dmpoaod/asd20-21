//
// Created by mikew on 2020-12-27.
//

#include "comm.h"
using namespace std;

BST::BST(int val)
{
    this->root = new comm::Node();
    this->root->val = val;
}


comm::Node* BST::insert(comm::Node* nde, comm::Node* x)
{
    if (nde == nullptr)
    {
        return x;
    }
    else if (x->val > nde->val)
    {
        nde->right = BST::insert(nde->right, x);
        return nde;
    }
    else
    {
        nde->left = BST::insert(nde->left, x);
        return nde;
    }
}

comm::Node* BST::insertwithpred(comm::Node* nde, comm::Node* x, comm::Node* pred = nullptr)
{
    if (nde == nullptr)
    {
        x->pred = pred;
        return x;
    }
    else if (x->val > nde->val)
    {
        nde->right = insertwithpred(nde->right, x, nde);
    }
    else
    {
        nde->left = insertwithpred(nde->left, x, nde);
    }
    return nde;
}

comm::Node* BST::search(comm::Node* nde, int val)
{
    if (nde == nullptr || val == nde->val)
        return nde;
    if (val > nde->val)
        return search(nde->right, val);
    return search(nde->left, val);
}

comm::Node* BST::del (comm::Node* nde, int val)
{
    if (nde == nullptr)
        return nde;

    if (val < nde->val)
    {
        nde->left = del(nde->left, val);
    }
    else if (val > nde->val)
    {
        nde->right = del(nde->right, val);
    }
    else
        {
        if (nde->left == nullptr)
        {
            comm::Node* temp = nde->right;
            delete nde;
            return temp;
        }
        else if (nde->right == nullptr)
        {
            comm::Node* temp = nde->left;
            delete nde;
            return temp;
        }

        comm::Node* temp = nde->right;

        while (temp->left != nullptr)
        {
            temp = temp->left;
        }

        nde->val = temp->val;

        nde->right = del(nde->right, temp->val);
    }
    return nde;
}

void BST::inorder (comm::Node* nde)
{
    if (nde == nullptr) return;
    inorder(nde->left);
    cout << nde->val << " ";
    inorder(nde->right);
}

bool BST::search(int val)
{
    if (search(this->root, val) == nullptr)
        return false;
    return true;
}

void BST::insert(int val)
{
    if (this->root == nullptr)
    {
        this->root = new comm::Node();
        this->root->val = val;
        return;
    }
    comm::Node* nde = new Node();
    nde->val = val;

    BST::insert(this->root, nde);
}


void BST::del(int val)
{
    BST::del(this->root, val);
}

int AVL::height(comm::Node* nde)
{
    if (nde == nullptr) return 0;
    return nde->height;
}

int AVL::max(int a, int b)
{
    return (a > b) ? a : b;
}

comm::Node* AVL::rRot(comm::Node *nde)
{
    comm::Node *x = nde->left;
    comm::Node *y = x->right;
    x->right = nde;
    nde->left = y;
    nde->height = max(height(nde->left), height(nde->right)) + 1;
    x->height = max(height(x->left), height(x->right)) + 1;

    return x;
}

comm::Node* AVL::lRot(comm::Node* nde)
{
    comm::Node *y = nde->right;
    comm::Node *ty = y->left;
    y->left = nde;
    nde->right = ty;
    nde->height = max(height(nde->left), height(nde->right)) + 1;
    y->height = max(height(y->left), height(y->right)) + 1;

    return y;
}

int AVL::Bal(comm::Node* nde)
{
    if (nde == nullptr)
        return 0;
    return height(nde->left) - height(nde->right);
}

bool AVL::search(int val)
{
    return this->bst->search(val);
}

void AVL::insert(int val)
{
    if (this->bst->root == nullptr)
    {
        this->bst->insert(val);
        return;
    }

    comm::Node* nde = new Node();
    nde->val = val;
    bst->insert(this->bst->root, nde);
    nde->height = max(height(nde->left), height(nde->right)) + 1;
    int bal = Bal(nde);

    if (bal > 1 && val < nde->left->val)
    {
        rRot(nde);
        return;
    }

    if (bal < -1 && val > nde->right->val)
    {
        lRot(nde);
        return;
    }

    if (bal > 1 && val > nde->left->val)
    {
        nde->left = lRot(nde->left);
        rRot(nde);
        return;
    }

    if (bal < -1 && val < nde->right->val)
    {
        nde->right = rRot(nde->right);
        lRot(nde);
        return;
    }
}

void AVL::del(int val)
{
    comm::Node* nde = this->bst->del(this->bst->root, val);

    if (nde == nullptr)
        return;

    nde->height = max(height(nde->left), height(nde->right)) + 1;
    int bal = Bal(nde);

    if (bal > 1 && Bal(nde->left) >= 0)
    {
        rRot(nde);
        return;
    }

    if (bal > 1 && Bal(nde->left) < 0)
    {
        nde->left = lRot(nde->left);
        rRot(nde);
        return;
    }

    if (bal < -1 && Bal(nde->right) <= 0)
    {
        lRot(nde);
        return;
    }

    if (bal < -1 && Bal(nde->right) > 0)
    {
        nde->right = rRot(nde->right);
        lRot(nde);
        return;
    }
}

bool RBT::search(int value)
{
    return this->bst->search(value);
}

void RBT::insert(int value)
{
    if (this->bst->root == nullptr)
    {
        comm::Node* node = new comm::Node();
        this->bst->root = node;
        node->value = value;
        node->color = 0;
        return;
    }

    comm::Node* node = new comm::Node();
    node->value = value;
    this->bst->insertwithpred (this->bst->root, node);
    inFixup(node);
}

void RBT::inFixup(comm::Node* node)
{
    comm::Node* u;
    while (node->pred->color == 1)
    {
        if (node->pred == node->pred->pred->right)
        {
            u = node->pred->pred->left;
            if (u->color == 1)
            {
                u->color = 0;
                node->pred->color = 0;
                node->pred->pred->color = 1;
                node = node->pred->pred;
            }
            else
            {
                if (node == node->pred->left)
                {
                    node = node->pred;
                    rRot(node);
                }

                node->pred->color = 0;
                node->pred->pred->color = 1;
                lRot (node->pred->pred);
            }
        }
        else
        {
            u = node->pred->pred->right;

            if (u->color == 1)
            {
                u->color = 0;
                node->pred->color = 0;
                node->pred->pred->color = 1;
                node = node->pred->pred;
            }
            else
            {
                if (node == node->pred->right)
                {
                    node = node->pred;
                    lRot (node);
                }

                node->pred->color = 0;
                node->pred->pred->color = 1;
                rRot(node->pred->pred);
            }
        }
        if (node == this->bst->root)
            break;
    }

    this->bst->root->color = 0;
}

void RBT::del (comm::Node* node, int k)
{
    comm::Node* z = nullptr;
    comm::Node* x;
    comm::Node* y;
    while (node != nullptr)
    {
        if (node->value == k)
        {
            z = node;
        }

        if (node->value <= k)
        {
            node = node->right;
        }
        else
        {
            node = node->left;
        }
    }

    if (z == nullptr)
        return;

    y = z;
    int y_original_color = y->color;

    if (z->left == nullptr)
    {
        x = z->right;
        Trp (z, z->right);
    }
    else if (z->right == nullptr)
    {
        x = z->left;
        Trp (z, z->left);
    }
    else
    {
        y = z->right;

        while (y->left != nullptr)
        {
            y = y->left;
        }

        y_original_color = y->color;
        x = y->right;

        if (y->pred == z)
            x->pred = y;
        else
        {
            Trp (y, y->right);
            y->right = z->right;
            y->right->pred = y;
        }

        Trp (z, y);
        y->left = z->left;
        y->left->pred = y;
        y->color = z->color;
    }

    delete z;

    if (y_original_color == 0)
    {
        delFixup (x);
    }
}

void RBT::delFixup (comm::Node *x)
{
    comm::Node* s;

    while (x != this->bst->root && x->color == 0)
    {
        if (x == x->pred->left)
        {
            s = x->pred->right;

            if (s->color == 1)
            {
                s->color = 0;
                x->pred->color = 1;
                lRot (x->pred);
                s = x->pred->right;
            }

            if ((s->left != nullptr && s->right != nullptr) && (s->left->color == 0 && s->right->color == 0))
                s->color = 1;
            else
            {
                if (s->right != nullptr && s->right->color == 0)
                {
                    s->left->color = 0;
                    s->color = 1;
                    rRot(s);
                    s = x->pred->right;
                }

                s->color = x->pred->color;
                x->pred->color = 0;
                s->right->color = 0;
                lRot (x->pred);
                x = this->bst->root;
            }
        }
        else
        {
            s = x->pred->left;

            if (s->color == 1)
            {
                s->color = 0;
                x->pred->color = 1;
                rRot(x->pred);
                s = x->pred->left;
            }

            if ((s->left != nullptr && s->right != nullptr) && (s->left->color == 0 && s->right->color == 0))
            {
                s->color = 1;
                x = x->pred;
            }
            else
            {
                if (s->left != nullptr && s->left->color == 0)
                {
                    s->right->color = 0;
                    s->color = 1;
                    lRot (s);
                    s = x->pred->left;
                }

                s->color = x->pred->color;
                x->pred->color = 0;
                s->left->color = 0;
                rRot(x->pred);
                x = this->bst->root;
            }
        }
    }
    x->color = 0;
}

void RBT::Trp (comm::Node *, comm::Node *)
{
    if (u->pred == nullptr)

        this->bst->root = v;
    else if (u == u->pred->left)
        u->pred->left = v;
    else
        u->pred->right = v;
    v->pred = u->pred;
}

void RBT::lRot (comm::Node* x)
{
    comm::Node* y = x->right;
    x->right = y->left;

    if (y->left != nullptr)
    {
        y->left->pred = x;
    }

    y->pred = x->pred;

    if (x->pred == nullptr)
        this->bst->root = y;
    else if (x == x->pred->left)
        x->pred->left = y;
    else
        x->pred->right = y;

    y->left = x;
    x->pred = y;
}

void RBT::rRot(comm::Node* x)
{
    comm::Node* y = x->left;
    x->left = y->right;

    if (y->right != nullptr)
    {
        y->right->pred = x;
    }

    y->pred = x->pred;

    if (x->pred == nullptr)
    {
        this->bst->root = y;
    }
    else if (x == x->pred->right)
    {
        x->pred->right = y;
    }
    else
    {
        x->pred->left = y;
    }

    y->right = x;
    x->pred = y;
}

void RBT::del (int value)
{
    del (this->bst->root, value);
}
