//
// Created by mikew on 2021-12-27.
//

#ifndef DRZEWA_COMM_H
#define DRZEWA_COMM_H

class comm {
protected:
    struct Node {
        int val;
        Node* pred;
        Node* left;
        Node* right;
        int height;
        int col;
    };

public:
    bool search (int) = 0;
    void insert (int) = 0;
    void del (int) = 0;

};

class BST : public comm {
    friend class AVL;
    friend class RBT;
private:
    Node* root;
    Node* insert (Node*, Node*);
    Node* insertwithpred(Node*, Node*, Node*);
    void inorder (Node*);
    Node* search (Node*, int);
    Node* del (Node*, int);

public:
    bool search (int);
    void insert (int);
    void del (int);
};

class AVL : public comm {
private:
    Bst* bst;
    int height(comm::Node*);
    int max(int, int);
    Node* rRot (comm::Node*);
    Node* lRot (comm::Node*);
    int Bal(comm:Node*);

public:
    bool search (int);
    void insert (int);
    void del (int);
};

class RBT : public comm {
private:
    Bst* bst;
    void inFixup(comm::Node*);
    void delFixup(comm::Node*);
    void lRot(comm::Node*);
    void rRot(comm::Node*);
    void Trp(comm::Node*, comm::Node*);
    void del(comm::Node*, int);

public:
    bool search (int);
    void insert (int);
    void del (int);

};

#endif //DRZEWA_COMM_H
